<?php 
	class Pages extends CI_Controller{
		
		function __construct(){
			parent::__construct();
			$this->load->model('user_model','',TRUE);
			$this->load->model('curso_model','',TRUE);
			$this->load->model('disciplina_model','',TRUE);
		}
		
		public function home($page = 'home'){
		
			if ( ! file_exists('application/views/pages/'.$page.'.php')){
				// Whoops, we don't have a page for that!
				show_404();
			}
			
		
			if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);

					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/'.$page, $data);
			}
			else{
				redirect('home', 'refresh');
			}

			$this->load->view('templates/footer', $data);
		}
		
		public function cadastro_form($page = 'cadastro_form'){
		
			if ( ! file_exists('application/views/pages/'.$page.'.php')){
				// Whoops, we don't have a page for that!
				show_404();
			}
			
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Nome', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('type_user', 'Tipo de Usuário', 'required');
			
		
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata['logged_in'];
				$data['user'] = $session_data;
				$data['title'] = ucfirst($page);

				
				if($this->form_validation->run() == FALSE){
					//Field validation failed.  User redirected to login page
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/'.$page, $data);
				}
				else{
					$name = $this->input->post('name');
					$email = $this->input->post('email');
					$username = $this->input->post('username');
					$password = $this->input->post('password');
					$type_user = $this->input->post('type_user');
					
					$this->user_model->insert_user($name, $email, $username, $password, $type_user);
					
					redirect('pages/lista_'. $type_user, 'refresh');
				}
			}
			else{
				redirect('cadastro_form', 'refresh');
			}

			$this->load->view('templates/footer', $data);
		}
		
		public function check_username($username){
			$username = $this->input->post('username');

			//query the database
			$result = $this->user_model->check_username_availability($username);

			if($result){
				$this->form_validation->set_message('check_username', 'Usuário já existe. Tente um número de usuário diferente');
				return false;
			}
			else{
				return TRUE;
			}
		}
	
		public function lista_academico($page = 'lista_academico'){
			
			if ( ! file_exists('application/views/pages/listagem/'.$page.'.php')){
				// Whoops, we don't have a page for that!
				show_404();
			}
			
		
			if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);
					
					$data['rows'] = $this->user_model->get_all_academicos();
					
					$aux = $this->session->flashdata('delete_success_message');
					if (isset($aux) && !empty($aux)){
						$data['delete_message'] = $this->session->flashdata('delete_success_message');
					}
					
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/listagem/'.$page, $data);
					
					$this->load->view('templates/footer', $data);
			}
			else{
				redirect('home', 'refresh');
			}
		}
		
		public function lista_professor($page = 'lista_professor'){
		
			if ( ! file_exists('application/views/pages/listagem/'.$page.'.php')){
				// Whoops, we don't have a page for that!
				show_404();
			}
			
		
			if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);
					
					$data['rows'] = $this->user_model->get_all_professores();
					
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/listagem/'.$page, $data);
					
					$this->load->view('templates/footer', $data);
			}
			else{
				redirect('home', 'refresh');
			}
		}
		
		public function lista_curso($page = 'lista_curso'){
		
			if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);
					
					$data['rows'] = $this->curso_model->get_all_cursos();
					
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/listagem/'.$page, $data);
					
					$this->load->view('templates/footer', $data);
			}
			else{
				redirect('home', 'refresh');
			}
		}
		
		public function get_disciplinas_curso(){
			$idCurso = $this->input->post('idCurso');
			
			$data['disciplinas_curso'] = $this->curso_model->get_disciplinas_curso($idCurso);
			
			$this->load->view('pages/table_disciplinas_cursos.php', $data);
		}
		
		public function lista_disciplina($page = 'lista_disciplina'){
		
			if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);
					
					$data['courses'] = $this->curso_model->get_all_cursos();
					
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/listagem/'.$page, $data);
					
					$this->load->view('templates/footer', $data);
			}
			else{
				redirect('home', 'refresh');
			}
		}
		
		public function excluir_academico($idUser, $idAcademico, $page='lista_academico'){
				
				$session_data = $this->session->userdata['logged_in'];
				$data['user'] = $session_data;
				$data['title'] = ucfirst($page);
				
				$this->user_model->delete_user($idUser, $idAcademico);
				$this->session->set_flashdata('delete_success_message', 'Acadêmico excluído com sucesso.');
				redirect('pages/lista_academico', 'refresh');
		}
		
		public function editar_curso($idCurso, $page='editar_curso'){
			
			$session_data = $this->session->userdata['logged_in'];
			$data['user'] = $session_data;
			
			$this->load->library('form_validation');

			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('idUser', 'Coordenador', 'required');
		
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata['logged_in'];
				$data['user'] = $session_data;
				$data['title'] = ucfirst($page);
				$data['curso'] = $this->curso_model->get_curso($idCurso);
				$data['professores'] = $this->user_model->get_all_professores();

				
				if($this->form_validation->run() == FALSE){
					//Field validation failed.  User redirected to login page
					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/'.$page, $data);
				}
				else{
					$name = $this->input->post('nome');
					$idUser = $this->input->post('idUser');
					
					$this->curso_model->update_curso($idCurso, $name, $idUser);
					
					redirect('pages/lista_curso', 'refresh');
				}
			}
			else{
				redirect('editar_curso', 'refresh');
			}

			$this->load->view('templates/footer', $data);
		}
		
		public function disciplinas_academico($page='disciplinas_academico'){
		
			$session_data = $this->session->userdata['logged_in'];
			$data['user'] = $session_data;
			$data['title'] = ucfirst($page);
			
			$idAcademico = $this->user_model->get_id_academico($session_data['id']);
			
			$data['rows'] = $this->disciplina_model->get_disciplinas_academico($idAcademico);

			$this->load->view('templates/header', $data);
			$this->load->view('templates/menu');		
			
			
			$this->load->view('pages/disciplinas_academico.php', $data);
			
			$this->load->view('templates/footer', $data);
		}
		
		public function editar_perfil(){
		}
	}
?>