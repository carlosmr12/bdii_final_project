<?php
	
	class VerifyLogin extends CI_Controller{
		function __construct(){
			parent::__construct();
			
			$this->load->model('user_model','',TRUE);
		}
		
		function index(){
		   //This method will have the credentials validation
		   $this->load->library('form_validation');

		   $this->form_validation->set_rules('username', 'Username', 'required');
		   $this->form_validation->set_rules('password', 'Password', 'required|callback_check_database');
		   $this->form_validation->set_rules('type_user', 'Tipo de Usuário', 'required');

		   if($this->form_validation->run() == FALSE){
			 //Field validation failed.  User redirected to login page
				$data['title'] = ucfirst('login');
				$this->load->view('templates/header', $data); 
				$this->load->view('login');
		   }
		   else{
			 //Go to private area
			 
			 redirect('pages/home', 'refresh');
		   }

		}

	function check_database($password) {
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');

		//query the database
		$result = $this->user_model->login($username, $password);

		if($result){
			$type_user = $this->input->post('type_user');
			
			$sess_array = array();
			foreach($result as $row){
				$sess_array = array(
					'id' => $row->idUser,
					'username' => $row->usuario,
					'nome' => $row->nome,
					'email' => $row->email,
					'ultimoAcesso' => $row->ultimoAcesso,
					'type_user' => $type_user,
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}
		else{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
}
?>