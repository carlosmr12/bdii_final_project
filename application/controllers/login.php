<?php

	class Login extends CI_Controller{
		
		public function __construct(){
			parent::__construct();
			$this->load->library('session');
		}
		
		public function index(){
			
			if($this->session->userdata('logged_in')){
					$page = 'home';
					$session_data = $this->session->userdata['logged_in'];
					$data['user'] = $session_data;
					$data['title'] = ucfirst($page);

					$this->load->view('templates/header', $data);
					$this->load->view('templates/menu');				
					$this->load->view('pages/'.$page, $data);
			}
			else{
				$this->load->helper(array('form'));
				$data['title'] = ucfirst('login');
				$this->load->view('templates/header', $data);
				$this->load->view('login');
				$this->load->view('templates/footer', $data);
			}

			
		}
		
		
		public function logout(){
			$this->session->unset_userdata('logged_in');
			redirect('login', 'refresh');
		}
	}

?>