<?php
	
	class User_model extends CI_Model{
		
		function login($username, $password){
			$this->db->select('idUser, nome, email, usuario, ultimoAcesso');
			$this->db->from('users');
			$this->db->where('usuario', $username);
			$this->db->where('senha', $password);
			$this->db->where('status', 0);
			
			$query = $this->db->get();
			
			if($query->num_rows() == 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function check_username_availability($username){
			$this->db->select('idUser');
			$this->db->from('users');
			$this->db->where('usuario', $username);
			
			$query = $this->db->get();
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function get_all_academicos(){
			$query = $this->db->query('SELECT * FROM get_users');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function get_all_professores(){
			$query = $this->db->query('SELECT * FROM get_professors');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		function delete_user($idUser, $idAcademico){
			
			$query = $this->db->query('UPDATE users u SET u.status=1 WHERE u.idUser= ' . $idUser);
			return TRUE;
		}
		
		function insert_user($name, $email, $username, $password, $type_user){
			
			$query0 = $this->db->query("INSERT INTO users (nome, usuario, senha, email, ultimoAcesso) VALUES ('" .$name ."','".$username."','".$password."','".$email."', CURRENT_TIMESTAMP())");
			
			$idUser = $this->db->query("SELECT MAX(idUSer) id FROM users")->result();
			$query1 = $this->db->query("INSERT INTO " . $type_user . " (idUser) VALUES (" . $idUser . ")");
			
			return True;
		}
		
		function get_id_academico($idUser){
			$query = $this->db->query('SELECT a.idAcademico FROM academicos a, users u WHERE u.idUser=a.idUser AND u.idUser='. $idUser.';' 	);
			
			return True;
		}
	};

?>