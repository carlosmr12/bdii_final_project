<?php
	
	class Disciplina_model extends CI_Model{
		
		function get_all_disciplinas(){
			$query = $this->db->query('SELECT d.nome as nome, d.cargaHoraria as carga_horaria FROM disciplinas d;');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function get_disciplinas_academico($idAcademico){
		
			$query = $this->db->query('CALL getAcademicoDisciplinas(' . $idAcademico . ')');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
	}

?>