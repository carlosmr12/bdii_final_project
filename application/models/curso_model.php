<?php
	
	class Curso_model extends CI_Model{
		
		function get_all_cursos(){
			$query = $this->db->query('SELECT c.idCurso as idCurso, c.nome as curso, u.nome as coordenador FROM cursos c INNER JOIN users u ON c.idUser = u.idUser;');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function get_disciplinas_curso($idCurso){
			$query = $this->db->query('SELECT d.nome as nome, d.cargaHoraria as carga_horaria FROM curso_disciplina c INNER JOIN disciplinas d ON d.idDisciplina = c.idDisciplina WHERE c.idCurso = '.$idCurso.';');
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
		function get_curso($idCurso){
			$query = $this->db->query('SELECT c.idCurso, c.nome, u.nome as coordenador, c.idUser FROM cursos c, users u WHERE u.idUser=c.idUser AND c.idCurso = ' . $idCurso);
			
			if($query->num_rows() >= 1){
				return $query->result();
			}
			else{
				return false;
			}
			
		}
		
		function update_curso($idCurso, $name, $idUser){
		
			$query = $this->db->query('CALL updateCourse(' . $idCurso . ', "' . $name . '", ' . $idUser . ')');
			
			return True;
		}
	}

?>