<div class='ui stripe'>
<div class='ui page grid'>
	<div class='column'>
		<h1>Lista de Cursos</h1>
		<table class="ui table segment">
				<thead>
					<tr><th>Nome</th>
					<th>Coordenador</th>
					<?php if ($user['type_user'] == 'administrador'){?>
					<th>Ações</th>
					<?php } ?>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($rows as $row){
				?>
					<tr>
						<td><?php echo $row->curso ?></td>
						<td><?php echo $row->coordenador ?></td>
						<?php if ($user['type_user'] == 'administrador'){?>
						<td>
							<a class="ui blue button" href="<?php echo $this->config->base_url() . 'index.php/pages/editar_curso/' . $row->idCurso . '/'?>">
								<i class="edit icon"></i>
								Editar
							</a>
						</td>
						<?php } ?>
					</tr>
				<?php
					}
				?>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>