<script type="text/javascript">
	$(document).ready(function(){
		$('i.close.icon').click(function(){
			$('.ui.success.message').hide();
		});
	})
</script>

<div class='ui stripe'>
	<div class='ui page grid'>
		<div class='column'>
			<?php
				if (isset($delete_message) && !empty($delete_message)){
			?>
				<div class="ui success message">
			<?php
					echo $delete_message;
			?>
					<i class="close icon"></i>
				</div>
			<?php
				}
			?>
			<h1>Lista de Acadêmicos</h1>
			
			<table class="ui table segment">
				<thead>
					<tr><th>Nome</th>
					<th>Curso</th>
					<th>Email</th>
					<th>Ações</th>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($rows as $row){
				?>
					<tr>
						<td><?php echo $row->nome ?></td>
						<td><?php echo $row->curso ?></td>
						<td><?php echo $row->email ?></td>
						<td>
							<a class="ui red button" href="<?php echo $this->config->base_url() . 'index.php/pages/excluir_academico/' . $row->idUser . '/' . $row->idAcademico?>">
								<i class="delete icon"></i>
								Excluir
							</a>
						</td>
					</tr>
				<?php
					}
				?>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>