<script type="text/javascript">
	$(document).ready(function(){
		$('#course').on('change',function(e){
			var id = $(this).val();
			$.ajax({
				'url' : "<?php echo site_url('pages/get_disciplinas_curso')?>",
				'type' : 'POST', //the way you want to send data to your URL
				'data': {'idCurso': id},
				'success' : function(data){ //probably this request will return anything, it'll be put in var "data"
					var container = $('#container-table'); //jquery selector (get element by id)
					if(data){
						container.html(data);
					}
				}
            });
			e.preventDefault();
		});
	})
</script>
<div class='ui stripe'>
	<div class='ui page grid'>
		<div class='column'>
			<h1>Lista de Disciplinas</h1>
			<div class="field">
				<label>Selecione o curso</label>
				<div class="ui selection">
					<select id='course'>
						<option selected value='0'>...</option>
						<?php foreach ($courses as $course){ ?>
						<option value="<?php echo $course->idCurso; ?>"><?php echo $course->curso; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<br />
			<div id='container-table'></div>
		</div>
	</div>
</div>