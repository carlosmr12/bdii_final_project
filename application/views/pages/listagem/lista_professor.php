<div class='ui stripe'>
<div class='ui page grid'>
	<div class='column'>
		<h1>Lista de Professores</h1>
		<table class="ui table segment">
				<thead>
					<tr><th>Nome</th>
					<th>Email</th>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($rows as $row){
				?>
					<tr>
					  <td><?php echo $row->nome ?></td>
					  <td><?php echo $row->email ?></td>
					</tr>
				<?php
					}
				?>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>