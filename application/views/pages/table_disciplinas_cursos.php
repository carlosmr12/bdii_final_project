<?php if(isset($disciplinas_curso) && !empty($disciplinas_curso)) : ?>
    <table class="ui table segment">
        <thead>
            <tr>
            	<th>Nome</th>
            	<th>Carga Horária</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($disciplinas_curso as $row) : ?>
                <tr>
                    <td><?php echo $row->nome; ?></td>
                    <td><?php echo $row->carga_horaria; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <h3>Não foram encontrados resultados!</h3>
<?php endif; ?>