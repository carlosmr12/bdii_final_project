<div class='ui stripe'>
	<div class='ui page grid'>
		<div class='column'>
			<div class="ui form segment">
				<?php if (validation_errors() != false){ ?>
					<div class="ui red message">
						<?php echo validation_errors(); ?>
					</div>
				<?php } ?>
				<?php echo form_open('pages/editar_curso/'.$curso[0]->idCurso); ?>
					<div class="field">
						<label>Nome</label>
						<div class="ui left labeled icon input">
							<input type="nome" placeholder="Email" name="nome" value="<?php echo $curso[0]->nome;?>" />
							<i class="tag icon"></i>
							<div class="ui corner label">
								<i class="icon asterisk"></i>
							</div>
						</div>
					</div>
					<div class="field">
						<label>Selecione o Coordenador</label>
						<div class="ui selection">
							<select id='coordenador' name='idUser'>
								<?php foreach ($professores as $user){ ?>
									<?php if ($user->idUser == $curso[0]->idUser){
										$selected = 'selected';
									}else{
										$selected = '';
									}?>
								<option <?php echo $selected; ?> value="<?php echo $user->idUser; ?>"><?php echo $user->nome; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<input class="ui blue submit button" type="submit" value="Salvar" />
				</form>
			</div>
		</div>
	</div>
</div>