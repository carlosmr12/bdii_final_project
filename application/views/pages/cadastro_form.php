<div class='ui stripe'>
	<div class='ui page grid'>
		<div class='column'>
			<div class="ui form segment">
				<?php if (validation_errors() != false){ ?>
					<div class="ui red message">
						<?php echo validation_errors(); ?>
					</div>
				<?php } ?>
				<?php echo form_open('pages/cadastro_form'); ?>
					<label>Nome</label>
						<div class="ui left labeled icon input">
							<input type="text" placeholder="Nome" name="name" />
							<i class="user icon"></i>
							<div class="ui corner label">
								<i class="icon asterisk"></i>
							</div>
						</div>
					<div class="field">
						<label>Email</label>
						<div class="ui left labeled icon input">
							<input type="email" placeholder="Email" name="email" />
							<i class="mail icon"></i>
							<div class="ui corner label">
								<i class="icon asterisk"></i>
							</div>
						</div>
					</div>
					<div class="field">
						<label>Username</label>
						<div class="ui left labeled icon input">
							<input type="text" placeholder="Username" name="username" />
							<i class="user icon"></i>
							<div class="ui corner label">
								<i class="icon asterisk"></i>
							</div>
						</div>
					</div>
					<div class="field">
						<label>Password</label>
						<div class="ui left labeled icon input">
							<input type="password" placeholder="Password" name="password" />
							<i class="lock icon"></i>
							<div class="ui corner label">
								<i class="icon asterisk"></i>
							</div>
						</div>
					</div>
					<div class="field">
						<label>Tipo de usuário</label>
						<div class="ui dropdown selection">
							<input type="hidden" name="type_user" />
							<div class="default text">...</div>
							<i class="dropdown icon"></i>
							<div class="menu">
								<div class="item" data-value="administrador">Administrador</div>
								<div class="item" data-value="academico">Acadêmico</div>
								<div class="item" data-value="professor">Professor</div>
							</div>
						</div>
					</div>
					<input class="ui blue submit button" type="submit" value="Salvar" />
				</form>
			</div>
		</div>
	</div>
</div>