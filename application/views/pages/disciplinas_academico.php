
<div class='ui stripe'>
	<div class='ui page grid'>
		<div class='column'>
			<h1>Minhas Disciplinas</h1>
			
			<table class="ui table segment">
				<thead>
					<tr><th>Nome</th>
					<th>Faltas</th>
					<th>Nota</th>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($rows as $row){
				?>
					<tr>
						<td><?php echo $row->nome ?></td>
						<td><?php echo $row->faltas ?></td>
						<td><?php echo $row->nota ?></td>
					</tr>
				<?php
					}
				?>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>