<div class='ui page grid'>
	<div class='column'>
		<div class="ui large inverted menu">
			<div class='title item'>
				<b>BD2</b>
			</div>
			<a class="active item" href="home">
				<i class="home icon"></i> Home
			</a>
			<?php if ($user['type_user'] == 'administrador'){?>
				<a class="item" href="<?php echo site_url('pages/cadastro_form'); ?>">
					<i class="edit icon"></i> Cadastros de Usuários
				</a>
			<?php }?>
			<div class="ui dropdown item">
				<i class="list layout icon"></i> Listagem <i class="dropdown icon"></i>
				<div class="menu">
					<?php if ($user['type_user'] != 'academico'){?>
					<div class="item">
						<a href="<?php echo site_url('pages/lista_academico'); ?>" style="text-decoration:none;"><i class="users icon"></i>Acadêmicos</a>
					</div>
					<?php }?>
					<div class="item">
						<a href="<?php echo site_url('pages/lista_professor'); ?>" style="text-decoration:none;"><i class="tasks icon"></i>Professores</a>
					</div>
					<div class="item">
						<a href="<?php echo site_url('pages/lista_curso'); ?>" style="text-decoration:none;"><i class="grid layout icon"></i>Cursos</a>
					</div>
					<div class="item">
						<a href="<?php echo site_url('pages/lista_disciplina'); ?>" style="text-decoration:none;"><i class="unordered list icon"></i>Disciplinas</a>
					</div>
				</div>
			</div>
			<?php if ($user['type_user'] == 'academico'){?>
				<a class="item" href="<?php echo site_url('pages/disciplinas_academico'); ?>">
					<i class="table icon"></i> Minhas Disciplinas
				</a>
			<?php } ?>
			<div class='right menu'>
				<div class='ui dropdown link item'>
					
					<?php
						if (isset($user) and $user != NULL){
					?>
					<i class="setting  icon"></i>
					<?php
						echo $user['nome'];
					}
					?>
					<i class="dropdown icon"></i>
					<div class="menu">
						<div class="item">
							<a  href="<?php echo site_url('pages/editar_perfil'); ?>" style="text-decoration:none;">
								<i class="user icon"></i>Editar Perfil
							</a>
						</div>
						<div class="item">
							<a  href="<?php echo site_url('login/logout'); ?>" style="text-decoration:none;">
								<i class="sign out icon"></i>Logout
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>