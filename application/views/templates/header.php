<html>

	<head>
		<title><?php echo $title ?></title>
		<meta charset="utf-8">
		<link rel='stylesheet' type='text/css' href="<?php echo (CSS.'semantic.css') ?>" />
		<link rel='stylesheet' type='text/css' href="<?php echo (CSS.'bootstrap.css') ?>" />
		<script type="text/javascript" src="<?php echo (JS. 'jquery-1.10.2.js')?>"></script>
		<script type="text/javascript" src="<?php echo (JS. 'semantic.js')?>"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.ui.dropdown').dropdown();
			});
		</script>
	</head>
	
	<body>
	
		