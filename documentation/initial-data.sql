INSERT INTO `users` (`nome`, `usuario`, `senha`, `email`, `ultimoAcesso`) VALUES
('Nilton', 'nilton128', 'root', 'nilton128@gmail.com', '2013-11-19 22:15:41'),
('Carlos Henrique', 'admin', 'carlos', 'carlos@gmail.com', '2013-11-19 22:19:29'),
('Leandro ', 'leandroA', 'galo', 'leandro.allmeida@yahoo.com.br', '2013-11-19 22:24:33'),
('ISABELA SOARES ', 'isabela1', '123', 'ISABELA@gmail.com', '2013-11-19 22:26:39'),
('MARIA THEREZA ', 'mariath', '741', 'MARIA_THEREZA@yahoo.com ', '2013-11-19 22:27:33'),
('PAULO AFONSO', 'pauloA', '7898', 'PAULOAFONSO@hotmail.com', '2013-11-19 22:28:28'),
('LUIZ FELIPE ', 'luizFe', '85621', 'LUIZFELIPE@gmail.com ', '2013-11-19 22:31:47'),
('PEDRO HENRIQUE', 'pedroHen', '74811', 'PEDROHENRIQUE@yahoo.com', '2013-11-19 22:39:21'),
('SABRINA DOS SANTOS', 'sabrinaSan', '748511', 'SABRINA99@hotmail.com', '2013-11-19 22:40:32'),
('DANIELE PEREIRA ', 'danielepe', '36584', 'DANIELEPEREIRA@gmail.com ', '2013-11-19 22:42:03'),
('RONALDO DOS SANTOS', 'ronaldosan', '32154', 'RONALDO94@gmail.com', '2013-11-19 22:43:11'),
('LUCIANA GONÇALVES', 'lucianagon', '12365', 'LUCIANA_GONÇALVES@gmail.com', '2013-11-19 22:44:45'),
('ROBERTA ALVES', 'robertaAl', '1452', 'ROBERTAALVES@gmail.com', '2013-11-19 22:45:30'),
('Luiz Carlos Pires ', 'luizcps', 'jpa_dao', 'luizcps@hotmail.com', '2013-11-19 22:50:59'),
('Nilton Alves Maia', 'niltonAlves', 'Tanenbaum', 'niltonmaia@santoagostinho.edu.br', '2013-11-19 22:56:27'),
('Castro Filho', 'castroF', '87548', 'Castro_Filho@gmail.com', '2013-11-19 23:21:11'),
('PEDRO IGOR', 'predroI', '78544', 'PEDRO_IGOR@gmail.com', '2013-11-19 23:28:18'),
('CRISTINA APARECIDA', 'cristinaAp', '216598', 'CRISTINA_APARECIDA@gmail.com', '2013-11-19 23:30:58'),
('JUNIA MARISE', 'juniaMa', '3658', 'JUNIA_MARISE@gmail.com', '2013-11-19 23:33:07'),
('KATHLEEN WELLEN ', 'kathleen', '54692', 'KATHLEEN_WELLEN@gmail.com ', '2013-11-19 23:36:43'),
('CLAUDIMAR OLIVEIRA', 'claudimar', '6589', 'CLAUDIMAR_OLIVEIRA@gmail.com', '2013-11-19 23:38:30'),
('Mateus', 'mateus123', '2121', 'Mateus12@gmail.com', '2013-11-19 18:45:02'),
('Roberto', 'roberto123', '33665', 'Roberto4@gmail.com', '2013-11-20 18:46:12'),
('Edson', 'edson123', '6544', 'edson5@gmail.com', '2013-11-20 18:48:45');


INSERT INTO `professores` (`idUser`) VALUES
(3),
(14),
(15);

INSERT INTO `disciplinas` (`nome`, `cargaHoraria`) VALUES
('Banco de dados II', '72'),
('Sistemas Distribuídos I', '72'),
('Desenvolvimento Web', '72'),
('Cardiologia', '80'),
('Historia antiga', '80'),
('Calculo', '72');

INSERT INTO `cursos` (`nome`, `idUser`) VALUES
('Sistemas de informacao', 3),
('Historia', 14),
('Matematica', 3),
('Medicina ', 15);


INSERT INTO `administradores` (`idUser`) VALUES
(1),
(2);

INSERT INTO `academicos` (`idUser`, `idCurso`) VALUES
(4, 1),
(5, 2),
(6, 3),
(7, 4),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(17, 3),
(18, 2),
(19, 4),
(20, 4),
(21, 3);

INSERT INTO `academico_disciplina` (`idAcademico`, `idDisciplina`, `nota`, `faltas`, `semestre`) VALUES
(1, 1, 80, 4, '1/2012'),
(2, 4, 91, 2, '1/2011'),
(3, 6, 70, 10, '1/2010'),
(4, 5, 78, 2, '2/2009'),
(5, 3, 89, 4, '2/2011'),
(6, 2, 75, 6, '1/2012'),
(7, 1, 72, 8, '2/2012'),
(8, 3, 80, 2, '1/2010'),
(9, 1, 59, 4, '2/2010'),
(10, 2, 68, 4, '2/2009'),
(11, 1, 56, 6, '2/2012'),
(12, 6, 43, 16, '1/2011'),
(13, 5, 78, 8, '2/2011'),
(15, 4, 69, 14, '1/2009');

INSERT INTO `curso_disciplina` (`idCurso`, `idDisciplina`, `semestre`) VALUES
(1, 1, '1/2010'),
(1, 2, '1/2012'),
(1, 3, '2/2011'),
(2, 5, '2/2010'),
(3, 6, '1/2011'),
(4, 4, '1/2010');

INSERT INTO `disciplina_professor` (`idDisciplina`, `idProfessor`, `semestre`) VALUES
(1, 1, '1/2010'),
(2, 2, '1/2011'),
(3, 3, '1/2012');