CREATE SCHEMA IF NOT EXISTS `bd2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bd2` ;

-- -----------------------------------------------------
-- Table `bd2`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`users` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `usuario` VARCHAR(45) NULL,
  `senha` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `ultimoAcesso` DATETIME NULL,
  `status` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`idUser`));


-- -----------------------------------------------------
-- Table `bd2`.`Admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`administradores` (
  `idAdmin` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NOT NULL UNIQUE,
  PRIMARY KEY (`idAdmin`),
    FOREIGN KEY (`idUser`)
    REFERENCES `bd2`.`users` (`idUser`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`cursos` (
  `idCurso` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `idUser` INT NOT NULL,
  PRIMARY KEY (`idCurso`),
    FOREIGN KEY (`idUser`)
    REFERENCES `bd2`.`users` (`idUser`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`disciplinas` (
  `idDisciplina` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `cargaHoraria` VARCHAR(45) NULL,
  PRIMARY KEY (`idDisciplina`));


-- -----------------------------------------------------
-- Table `bd2`.`Academico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`academicos` (
  `idAcademico` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NOT NULL UNIQUE,
  `idCurso` INT NOT NULL,
  PRIMARY KEY (`idAcademico`),
    FOREIGN KEY (`idUser`)
    REFERENCES `bd2`.`users` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`idCurso`)
    REFERENCES `bd2`.`cursos` (`idCurso`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`professores` (
  `idProfessor` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NOT NULL UNIQUE,
  PRIMARY KEY (`idProfessor`),
    FOREIGN KEY (`idUser`)
    REFERENCES `bd2`.`users` (`idUser`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Disciplina_Professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`disciplina_professor` (
  `idDisciplina` INT NOT NULL,
  `idProfessor` INT NOT NULL,
  `semestre` VARCHAR(51) NULL,
  PRIMARY KEY (`idDisciplina`, `idProfessor`),
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `bd2`.`disciplinas` (`idDisciplina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`idProfessor`)
    REFERENCES `bd2`.`professores` (`idProfessor`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Curso_Disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`curso_disciplina` (
  `idCurso` INT NOT NULL,
  `idDisciplina` INT NOT NULL,
  `semestre` VARCHAR(51) NULL,
  PRIMARY KEY (`idCurso`, `idDisciplina`),
    FOREIGN KEY (`idCurso`)
    REFERENCES `bd2`.`cursos` (`idCurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `bd2`.`disciplinas` (`idDisciplina`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `bd2`.`Academico_Disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd2`.`academico_disciplina` (
  `idAcademico` INT NOT NULL,
  `idDisciplina` INT NOT NULL,
  `nota` DOUBLE NULL,
  `faltas` INT NULL,
  `semestre` VARCHAR(51) NULL,
  PRIMARY KEY (`idAcademico`, `idDisciplina`),
    FOREIGN KEY (`idAcademico`)
    REFERENCES `bd2`.`academicos` (`idAcademico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `bd2`.`disciplinas` (`idDisciplina`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
CREATE TABLE IF NOT EXISTS `bd2`.`log_system` (
	`idLog` INT(11) NOT NULL AUTO_INCREMENT,
	`idUser` INT(11) NOT NULL,
	`dateTime` DATETIME NULL DEFAULT NULL,
	`descricao` VARCHAR(45) NULL DEFAULT NULL,
	PRIMARY KEY (`idLog`),
	FOREIGN KEY (`idUser`)
	REFERENCES `bd2`.`users` (`idUser`)
	ON DELETE CASCADE
	ON UPDATE CASCADE);

DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `bd2`.`log_inserir_user`$$

CREATE
    /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER `bd2`.`log_inserir_user` AFTER INSERT ON `bd2`.`users` 
    FOR EACH ROW BEGIN
		INSERT INTO `log_system` (`dateTime`, `descricao`, `idUser`) VALUES (CURRENT_TIMESTAMP(), 'inseriu usuario', NEW.idUser);
    END;
$$

DELIMITER ;

DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    TRIGGER `log_excluir_user` AFTER UPDATE ON  `users`
	FOR EACH ROW 
	IF OLD.status =1 THEN 
		INSERT INTO log_system(  `dateTime` ,  `descricao` ,  `idUser` ) VALUES (CURRENT_TIMESTAMP(),'alterou o status do usuario', OLD.idUser);
	END IF;
$$

DELIMITER ;

CREATE VIEW get_users (idUser, idAcademico, nome, email, curso) AS
	SELECT DISTINCT(u.idUser), a.idAcademico, u.nome as nome, u.email as email, c.nome as curso 
	FROM users u 
	INNER JOIN academicos a ON a.idUser = u.idUser 
	INNER JOIN cursos c on a.idCurso = c.idCurso  AND u.status = 0;
	

CREATE VIEW get_professors (idUser, idProfessor, nome, email) AS
	SELECT DISTINCT(u.idUser), p.idProfessor, u.nome, u.email 
	FROM users u 
	INNER JOIN professores p ON p.idUser = u.idUser AND u.status = 0;
	
DELIMITER $$
	CREATE DEFINER=`root`@`localhost` PROCEDURE `updateCourse`(
	IN `param1` INT,
	IN `param2` VARCHAR(51),
	IN `param3` INT)
	BEGIN
		UPDATE cursos SET nome=param2, idUser=param3 WHERE idCurso=param1;
	END$$

DELIMITER ;

DELIMITER $$
	CREATE DEFINER=`root`@`localhost` PROCEDURE `getAcademicoDisciplinas`(
	IN `param1` INT)
	BEGIN
		SELECT d.nome, ad.faltas, ad.nota, ad.semestre
		FROM academico_disciplina ad, disciplinas d
		WHERE d.idDisciplina=ad.idDisciplina AND ad.idAcademico=param1;
	END$$

DELIMITER ;