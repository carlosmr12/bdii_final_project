

Sistema acadêmico

Banco de dados - OK
Diagrama com especificações de classes e atributos - OK

Interface - OK
	Tela de Login (trigger)
	Registro de aluno, professor, curso, disciplina (stored procedure)
	Alteração de aluno, professor, curso, disciplina
	Listagem de alunos (com busca por nome) (view)
	Listagem de disciplinas (com busca por nome) (view)
	Listagem de professores por disciplina (view)
	Listagem de disciplinas cursadas por um acadêmico (view)
	Exclusão de aluno, professor, curso, disciplina (stored procedure)	
	Tela de log do sistema (trigger)

Povoar o bd - OK

Documentação - 